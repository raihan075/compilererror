﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CompileError.Model.Model;
using CompileError.DatabaseContext.DatabaseContext;

namespace CompileError.Repository.Repository
{
    public class CategoryRepository
    {
        ProjectDbContext _dbContext = new ProjectDbContext();

        public bool Add(CategoryModel category)
        {
            _dbContext.Categories.Add(category);
            return _dbContext.SaveChanges()>0;
            
        }

        public bool Delete(int id)
        {
            CategoryModel aCategory = _dbContext.Categories.FirstOrDefault(c => c.Id == id);
            _dbContext.Categories.Remove(aCategory);
            return _dbContext.SaveChanges() > 0;
        }

        public bool Update(CategoryModel category)
        {
            CategoryModel aCategory = _dbContext.Categories.FirstOrDefault(c => c.Id == category.Id);
            if (aCategory != null)
            {
                aCategory.Code = category.Code;
                aCategory.Name = category.Name;
            }
            return _dbContext.SaveChanges() > 0;
        }

        public List<CategoryModel> ShowAll()
        {
           return _dbContext.Categories.ToList();
        }
        
        public CategoryModel Search(int id)
        {
            return _dbContext.Categories.FirstOrDefault(c => c.Id == id);
        }
    }
}
