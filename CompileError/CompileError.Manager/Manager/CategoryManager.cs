﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CompileError.Model.Model;
using CompileError.Repository.Repository;

namespace CompileError.Manager.Manager
{
    public class CategoryManager
    {
        CategoryRepository _categoryRepository = new CategoryRepository();

        public bool Add(CategoryModel category)
        {
            return _categoryRepository.Add(category);
        }

        public bool Delete(int id)
        {
            return _categoryRepository.Delete(id);
        }

        public bool Update(CategoryModel category)
        {
            return _categoryRepository.Update(category);
        }

        public List<CategoryModel> ShowAll()
        {
            return _categoryRepository.ShowAll();
        }

        public CategoryModel Search(int id)
        {
            return _categoryRepository.Search(id);
        }
    }
}
