﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompileError.Model.Model;
using CompileError.Manager.Manager;

namespace CompileError.Console
{
    class Program
    {
        private static readonly ProductManager _productManager = new ProductManager();
        static void Main(string[] args)
        {
            ProductModel productModel = new ProductModel
            {
                Category = "Mobile",
                Code = "m-1000",
                Name = "Samsung Galaxy Y",
                ReorderLevel = 100,
                Description = "First Generation Galaxy Phone"
            };

            _productManager.Add(productModel);
        }
    }
}
